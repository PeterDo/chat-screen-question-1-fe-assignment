import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import classNames from 'classnames';
import './ChatboxUpdate.scss';

const ChatboxUpdate = forwardRef( ({className, data: message}, ref) => {

  const chatboxUpdateClass = classNames({
    [className]: true,
    'chat-box-update': true
  });

  return (
    <div className={chatboxUpdateClass}>
      <div className="created-date">{Moment.utc(message.createdAt).local().format('ddd, MMM DD')}</div>
      <div className="content-text">{message.content && message.content.text}</div>
    </div>
  );
});

ChatboxUpdate.propTypes = {
  className: PropTypes.string,
  message: PropTypes.shape({
    content: PropTypes.shape({
      text: PropTypes.string
    })
  })
};

ChatboxUpdate.defaultProps = {
  className: '',
  message: {
    content: {
      text: ''
    }
  }
};

export default ChatboxUpdate;
