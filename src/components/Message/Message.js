import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import classNames from 'classnames';
import { MESSAGE_TYPE, CURRENT_USER } from '../../consts';
import './Message.scss';

const Message = forwardRef( ({className, data: message}, ref) => {

  const currentUserClass = classNames({
    'current-user': message.senderId === CURRENT_USER.ID
  });

  const horizontalAlignClass = classNames({
    [className]: true,
    'horizontal-align': true
  }, currentUserClass);

  const messageItemClass = classNames({
    'message-item': true,
  }, currentUserClass);

  const userLogoClass = classNames({
    'user-logo': true
  }, currentUserClass);

  const messageClass = classNames({
    message: true
  }, currentUserClass);

  const userNameClass = classNames({
    'user-name': true
  }, currentUserClass);
  const updatedDateClass = classNames({
    'updated-date': true
  }, currentUserClass);

  const contentTextClass = classNames({
    'content-text': true
  }, currentUserClass);
  
  return (
    <div className={horizontalAlignClass}>
      <div className={messageItemClass}>
        <img className={userLogoClass} alt="logo" src={message.sender && message.sender.picture}/>
        <div className={messageClass}>
          <div className="name-message-text">
            <div className={userNameClass}>{message.sender && message.sender.firstName}</div>
            <div className="message-text">
              {message.content && message.content.hasOwnProperty('url') && message.content.url && message.type === MESSAGE_TYPE.PHOTO ? 
              <div className="content-url" style={{backgroundImage: `url(${message.content.url})`}}/> : 
              <span className={contentTextClass}>{message.content.text}</span>}
              <span className={updatedDateClass}>{Moment.utc(message.updatedAt).local().format('h: mm A')}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

Message.propTypes = {
  className: PropTypes.string,
  message: PropTypes.shape({
    senderId: PropTypes.number,
    sender: PropTypes.shape({
      picture: PropTypes.string,
      firstName: PropTypes.string
    }),
    content: PropTypes.shape({
      url: PropTypes.string,
      text: PropTypes.string
    })
  })
};

Message.defaultProps = {
  className: '',
  message: {
    senderId: null,
    sender: {
      picture: '',
      firstName: ''
    },
    content: {
      url: '',
      text: ''
    }
  }
};

export default Message;
