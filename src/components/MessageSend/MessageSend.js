import React, { forwardRef } from 'react';
import './MessageSend.scss';

const MessageSend = forwardRef( ({}, ref) => {

  return (
    <div className="message-to-send">
      <div className="photo-text-container">
        <div className="photo">
          <label htmlFor="photo-upload" className="photo-label">
            <input style={{display: 'none'}} type="file" accept='image/*' id="photo-upload" />
            <i className="fas fa-camera camera-icon" />
          </label>
        </div>
        <input className="message-content" type="text" placeholder="Type message"/>
      </div>
      <div className="send-btn">
        <i className="fas fa-paper-plane plane-icon" data-fa-transform="rotate-45" />
      </div>
    </div>
  );
});

MessageSend.propTypes = {};

MessageSend.defaultProps = {};

export default MessageSend;
