import React, { useState, useEffect, Fragment } from 'react';
import { MESSAGE_TYPE } from './consts';
import chatService from './services/ChatService';
import { Message, ChatboxUpdate, MessageSend } from './components';
import './App.scss';

const App = () => {

  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const messages =  chatService.getMessages();
    const messagesByLatestDate = messages.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
    setMessages(messagesByLatestDate);
  }, []);

  return (
    <div id="q1-chatting">
      <div className="chat-group">
        Tennis buddies
        <div className="triangular" />
      </div>
      <div className="message-container">
        {messages.map((message) => (
          <Fragment key={message.id}>
            {message.type === MESSAGE_TYPE.SYSTEM ? 
            <ChatboxUpdate key={message.id} className="system" data={message} /> : 
            <Message key={message.id} className="item" data={message} />}
          </Fragment>
        ))}
      </div>
      <MessageSend />
    </div>
  );
}

export default App;
