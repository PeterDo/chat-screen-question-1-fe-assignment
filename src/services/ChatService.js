import dataChat from '../assets/files/Chats.json';

export default {
    getMessages() {
        if(dataChat){
            return dataChat.messages;
        }
        return null;
    }
};
